package fr.sc.api.armes.Events;

import fr.sc.api.armes.Enums.BulletTypes;
import fr.sc.api.armes.Enums.GunType;
import fr.sc.api.armes.Guns;
import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;

public class BulletHit implements Listener {

    private Guns plugin;

    public BulletHit(Guns sw)
    {
        this.plugin = sw;
    }

    @EventHandler
    public void onBulletHit(ProjectileHitEvent event)
    {
        if ((event.getEntity().getShooter() instanceof Player)) {

            int ticksLived = event.getEntity().getTicksLived();
            int id = event.getEntity().getEntityId();

            double damage = -1.0D;
            double expload = 0.0D;

            BulletTypes bulletType = BulletTypes.ARROW;
            Player shooter = (Player)event.getEntity().getShooter();
            Vector velocity = event.getEntity().getVelocity();
            GunType gun = GunType.GUN;
            if ((ticksLived > 0) && (ticksLived < 9000)) {
                gun = GunType.GUN;
            }
            if ((ticksLived > 10000) && (ticksLived < 19000)) {
                gun = GunType.RPG;
            }
            if ((ticksLived > 20000) && (ticksLived < 29000)) {
                gun = GunType.SHOTGUN;
            }
            if ((ticksLived > 30000) && (ticksLived < 39000)) {
                gun = GunType.GRENADE;
            }
            if (this.plugin.damages.containsKey(Integer.valueOf(id)))
            {
                damage = ((Double)this.plugin.damages.get(Integer.valueOf(id))).doubleValue();
                this.plugin.damages.remove(Integer.valueOf(id));
            }
            if (this.plugin.explosions.containsKey(Integer.valueOf(id)))
            {
                expload = ((Double)this.plugin.explosions.get(Integer.valueOf(id))).doubleValue();
                this.plugin.explosions.remove(Integer.valueOf(id));
            }
            if ((event.getEntity() instanceof Egg)) {
                bulletType = BulletTypes.EGG;
            }
            if ((event.getEntity() instanceof Arrow)) {
                bulletType = BulletTypes.ARROW;
            }
            if ((event.getEntity() instanceof EnderPearl)) {
                bulletType = BulletTypes.ENDER_PEARL;
            }
            if ((event.getEntity() instanceof Snowball)) {
                bulletType = BulletTypes.SNOWBALL;
            }
            if (damage != -1.0D)
            {
                GunHitEvent event2 = new GunHitEvent(gun, velocity, shooter, damage, bulletType, expload, event.getEntity());
                Bukkit.getServer().getPluginManager().callEvent(event2);
                if ((!event2.isCancelled()) && (event2.getExplosion() > 0.0D)) {
                    event.getEntity().getWorld().createExplosion(event.getEntity().getLocation(), (float)event2.getExplosion());
                }
            }
        }
    }

    @EventHandler
    public void hitEntity(EntityDamageByEntityEvent event)
    {
        if (((event.getDamager() instanceof Projectile)) &&
                ((((Projectile)event.getDamager()).getShooter() instanceof Player)))
        {
            int ticksLived = event.getDamager().getTicksLived();
            int id = event.getDamager().getEntityId();
            double damage = -1.0D;
            double expload = 0.0D;
            BulletTypes bulletType = BulletTypes.ARROW;
            Player shooter = (Player)((Projectile)event.getDamager()).getShooter();
            Vector velocity = event.getDamager().getVelocity();
            GunType gun = GunType.GUN;
            Entity entity = event.getEntity();
            if ((ticksLived > 0) && (ticksLived < 9000)) {
                gun = GunType.GUN;
            }
            if ((ticksLived > 10000) && (ticksLived < 19000)) {
                gun = GunType.RPG;
            }
            if ((ticksLived > 20000) && (ticksLived < 29000)) {
                gun = GunType.SHOTGUN;
            }
            if ((ticksLived > 30000) && (ticksLived < 39000)) {
                gun = GunType.GRENADE;
            }
            if (this.plugin.damages.containsKey(Integer.valueOf(id)))
            {
                damage = ((Double)this.plugin.damages.get(Integer.valueOf(id))).doubleValue();
                this.plugin.damages.remove(Integer.valueOf(id));
            }
            if (this.plugin.explosions.containsKey(Integer.valueOf(id)))
            {
                expload = ((Double)this.plugin.explosions.get(Integer.valueOf(id))).doubleValue();
                this.plugin.explosions.remove(Integer.valueOf(id));
            }
            if ((event.getDamager() instanceof Egg)) {
                bulletType = BulletTypes.EGG;
            }
            if ((event.getDamager() instanceof Arrow)) {
                bulletType = BulletTypes.ARROW;
            }
            if ((event.getDamager() instanceof EnderPearl)) {
                bulletType = BulletTypes.ENDER_PEARL;
            }
            if ((event.getDamager() instanceof Snowball)) {
                bulletType = BulletTypes.SNOWBALL;
            }
            if (damage != -1.0D)
            {
                GunHitEntityEvent e = new GunHitEntityEvent(gun, velocity, shooter, damage, bulletType, expload, entity, event.getDamager());
                Bukkit.getServer().getPluginManager().callEvent(e);
                if (e.isCancelled()) {
                    event.setCancelled(true);
                }
                if (!e.isCancelled())
                {
                    if ((e.getEntity() instanceof LivingEntity)) {
                        ((LivingEntity)e.getEntity()).damage(e.getDamage());
                    }
                    if (e.getExplosion() > 0.0D) {
                        event.getEntity().getWorld().createExplosion(event.getEntity().getLocation(), (float)e.getExplosion());
                    }
                }
            }
        }
    }
}
