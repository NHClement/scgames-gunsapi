package fr.sc.api.armes;

import fr.sc.api.armes.Enums.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * API permettant de crée des armes facilement!
 * (Javadoc bientôt...)
 *
 * @author: NHClement
 */
public class Guns{

    private static HashMap<String, Guns> gunList = new HashMap<String, Guns>();

    public static HashMap<Guns, Integer> munition = new HashMap<Guns, Integer>();
    public static HashMap<Integer, Double> damages = new HashMap<Integer, Double>();
    public static HashMap<Integer, Double> explosions = new HashMap<Integer, Double>();

    private GunType gunType = GunType.GUN;
    private Player p = null;
    private ItemStack gunItem = new ItemStack(Material.BLAZE_ROD);
    private BulletTypes bulletType = BulletTypes.SNOWBALL;
    private MunitionType munitionType = MunitionType.NORMAL;
    private DamageType damageType = DamageType.MIDDLE;
    private ReloadTimes reloadTime = ReloadTimes.NORMAL;
    private ExplosionType explosionType = ExplosionType.NONE;
    private BulletVelocity bulletVelocity = BulletVelocity.NORMAL;
    private Integer munitions = 0;

    /**
     * Crée une arme.
     *
     * @param gun Voir l'énumération GunType. (GUN ou SHOTGUN ou RPG)
     */
    public Guns(GunType gun, Player p, ItemStack gi, BulletVelocity bv, BulletTypes bt, MunitionType mt, DamageType dt, ReloadTimes rt, ExplosionType et) {
        this.gunType = gun;
        this.gunItem = gi;
        this.bulletVelocity = bv;
        this.munitionType = mt;
        this.damageType = dt;
        this.reloadTime = rt;
        this.explosionType = et;
    }

    // -------------------
    // |     SETTERS     |
    // -------------------

    public ItemStack createGun(){
        gunList.put(gunItem.getItemMeta().getDisplayName(), new Guns(gunType, p, gunItem, bulletVelocity, bulletType, munitionType, damageType, reloadTime, explosionType));
        return getGunItem();
    }

    public void setGunType(GunType gun){
        this.gunType = gun;
    }

    public void setPlayer(Player p){
        this.p = p;
    }

    public void setGunItem(ItemStack is){
        this.gunItem = is;
    }

    public void setBulletVelocity(BulletVelocity bv){
        this.bulletVelocity = bv;
    }

    public void setBulletType(BulletTypes bt){
        this.bulletType = bt;
    }

    public void setMunitionType(MunitionType mt){

        if(mt == MunitionType.SUPER_LOW){
            this.munitions = 25;
        }else if(mt == MunitionType.LOW){
            this.munitions = 50;
        }else if(mt == MunitionType.NORMAL){
            this.munitions = 100;
        }else if(mt == MunitionType.HIGH){
            this.munitions = 200;
        }else if(mt == MunitionType.SUPER_HIGH){
            this.munitions = 300;
        }

        this.munitionType = mt;

    }

    public void setMunition(Guns g){
        this.munition.put(g, getMaxMunition(g));
    }

    public void removeMunition(Guns g){
        this.munition.put(g, getMunition(g) - 1);
    }

    public void setDamage(DamageType dt){
        this.damageType = dt;
    }

    public void setReloadTime(ReloadTimes rt){
        this.reloadTime = rt;
    }

    public void setExplosionType(ExplosionType et){
        this.explosionType = et;
    }

    // -------------------
    // |     GETTERS     |
    // -------------------

    public static HashMap<String, Guns> getGuns(){
        return gunList;
    }

    public GunType getGunType(){
        return this.gunType;
    }

    public Player getPlayer(){
        return this.p;
    }

    public ItemStack getGunItem(){
        return this.gunItem;
    }

    public BulletVelocity getBulletVelocity(){
        return this.bulletVelocity;
    }

    public BulletTypes getBulletType(){
        return this.bulletType;
    }

    public MunitionType getMunitionType(){
        return this.munitionType;
    }

    public DamageType getDamage(){
        return this.damageType;
    }

    public ReloadTimes getReloadTime(){
        return this.reloadTime;
    }

    public ExplosionType getExplosionType(){
        return this.explosionType;
    }

    public Integer getMunition(Guns g){

        return this.munition.get(g);

    }

    public Integer getMaxMunition(Guns g){

        if(g.getMunitionType() == MunitionType.SUPER_LOW){
            return 25;
        }else if(g.getMunitionType() == MunitionType.LOW){
            return 50;
        }else if(g.getMunitionType() == MunitionType.NORMAL){
            return 100;
        }else if(g.getMunitionType() == MunitionType.HIGH){
            return 200;
        }else if(g.getMunitionType() == MunitionType.SUPER_HIGH){
            return 300;
        }

        return 0;

    }

}
