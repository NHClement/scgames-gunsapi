package fr.sc.api.armes.Events;

import fr.sc.api.armes.Guns;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class Explosion implements Listener {

    private Guns plugin;

    public Explosion(Guns sw)
    {
        this.plugin = sw;
    }

    @EventHandler
    public void onPlayerTeleportByEnderpearl(PlayerTeleportEvent event)
    {
        if ((event.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL))) {
            event.setCancelled(true);
        }
    }
}
