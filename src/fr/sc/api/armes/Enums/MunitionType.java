package fr.sc.api.armes.Enums;

public enum  MunitionType {

    SUPER_LOW, LOW, NORMAL, HIGH, SUPER_HIGH;

}
