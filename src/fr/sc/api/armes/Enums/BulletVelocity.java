package fr.sc.api.armes.Enums;

public enum  BulletVelocity {

    SUPER_LOW, LOW, NORMAL, HIGH, SUPER_HIGH, SNIPER;

}
