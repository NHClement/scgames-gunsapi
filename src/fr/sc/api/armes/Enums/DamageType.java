package fr.sc.api.armes.Enums;

public enum  DamageType {

    SUPER_LOW, LOW, MIDDLE, HIGH, SUPER_HIGH, ONE_SHOT;

}
