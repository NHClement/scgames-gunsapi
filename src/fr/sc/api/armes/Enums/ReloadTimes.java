package fr.sc.api.armes.Enums;

public enum  ReloadTimes {

    INSTANT, SUPER_SLOW, SLOW, NORMAL, FAST, SUPER_FAST;

}
