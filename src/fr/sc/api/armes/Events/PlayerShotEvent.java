package fr.sc.api.armes.Events;

import fr.sc.api.armes.Enums.GunType;
import fr.sc.api.armes.Guns;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class PlayerShotEvent implements Listener {

    @EventHandler
    public void onPlayerClickEvent(PlayerInteractEvent e){

        Player p = e.getPlayer();
        ItemStack is = e.getItem();
        String gunName = is.getItemMeta().getDisplayName();
        HashMap<String, Guns> guns = Guns.getGuns();

        if(guns.keySet().contains(gunName)){
            Guns gun = guns.get(gunName);
            if(gun.getGunType() == GunType.GUN){

                ShootGun sh = new ShootGun(gun);
                sh.shootGun(gun, p, gun.getBulletType(), gun.getDamage(), gun.getBulletVelocity());

            }
            if(gun.getGunType() == GunType.GRENADE){

                // TODO

            }
            if(gun.getGunType() == GunType.RPG){

                // TODO

            }
            if(gun.getGunType() == GunType.SHOTGUN){

                // TODO

            }
        }

    }
}
