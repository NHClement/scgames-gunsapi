package fr.sc.api.armes.Events;

import fr.sc.api.armes.Enums.BulletTypes;
import fr.sc.api.armes.Enums.BulletVelocity;
import fr.sc.api.armes.Enums.DamageType;
import fr.sc.api.armes.Guns;
import fr.sc.api.util.ActionBarUtils;
import org.bukkit.Sound;
import org.bukkit.entity.*;

public class ShootGun {

    private Guns plugin;

    public ShootGun(Guns sw)
    {
        this.plugin = sw;
    }

    public void shootGun(Guns g, Player p, BulletTypes bulletType, DamageType damageType, BulletVelocity bulletVelocity) {

        if (bulletType.equals(BulletTypes.EGG)) {

            Egg EG = (Egg)p.launchProjectile(Egg.class);

            EG.setShooter(p);
            EG.setTicksLived(1);

            setVelocity(EG, bulletVelocity);
            setDamages(EG, damageType);

            p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50.0F, 30.0F);

        }
        if (bulletType.equals(BulletTypes.ARROW)) {

            Arrow A = (Arrow)p.launchProjectile(Arrow.class);

            A.setShooter(p);
            A.setTicksLived(1);

            setVelocity(A, bulletVelocity);
            setDamages(A, damageType);

            p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50.0F, 30.0F);

        }

        if (bulletType.equals(BulletTypes.ENDER_PEARL)) {

            EnderPearl E = (EnderPearl)p.launchProjectile(EnderPearl.class);

            E.setShooter(p);
            E.setTicksLived(1);

            setVelocity(E, bulletVelocity);
            setDamages(E, damageType);

            p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50.0F, 30.0F);

        }
        if (bulletType.equals(BulletTypes.SNOWBALL)) {

            Snowball SB = (Snowball)p.launchProjectile(Snowball.class);

            setVelocity(SB, bulletVelocity);

            SB.setShooter(p);
            SB.setTicksLived(1);

            setDamages(SB, damageType);

            p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50.0F, 30.0F);

        }

        g.removeMunition(g);

        ActionBarUtils.sendMessageToPlayer(p, "§c§l" + g.getGunItem().getItemMeta().getDisplayName() + " §7§o: §7(§a§l" + g.getMunition(g) + "§7/§a§l" + g.getMaxMunition(g) + "§7)");

        if(Reload.isReloadNecessary(g) == true){
            Reload.sendReload(g);
        }

    }

    private void setDamages(Entity e, DamageType damageType){

        if(damageType == DamageType.SUPER_LOW){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(1.0));

        }else if(damageType == DamageType.LOW){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(2.0));

        }else if(damageType == DamageType.MIDDLE){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(4.0));
            
        }else if(damageType == DamageType.HIGH){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(6.0));

        }else if(damageType == DamageType.SUPER_HIGH){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(7.0));

        }else if(damageType == DamageType.ONE_SHOT){

            this.plugin.damages.put(Integer.valueOf(e.getEntityId()), Double.valueOf(40.0));

        }

    }

    private void setVelocity(Entity e, BulletVelocity bulletVelocity){

        if(bulletVelocity == BulletVelocity.SUPER_LOW){

            e.setVelocity(e.getVelocity().multiply(1.0));

        }else if(bulletVelocity == BulletVelocity.LOW){

            e.setVelocity(e.getVelocity().multiply(2.0));

        }else if(bulletVelocity == BulletVelocity.NORMAL){

            e.setVelocity(e.getVelocity().multiply(2.5));

        }else if(bulletVelocity == BulletVelocity.HIGH){

            e.setVelocity(e.getVelocity().multiply(3.0));

        }else if(bulletVelocity == BulletVelocity.SUPER_HIGH){

            e.setVelocity(e.getVelocity().multiply(4));

        }else if(bulletVelocity == BulletVelocity.SNIPER){
            e.setVelocity(e.getVelocity().multiply(10.0));

        }

    }

}
