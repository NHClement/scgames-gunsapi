package fr.sc.api.armes.Events;

import fr.sc.api.SCApi;
import fr.sc.api.armes.Enums.ReloadTimes;
import fr.sc.api.armes.Guns;
import fr.sc.api.util.TitleUtils;
import fr.sc.api.util.UtilItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Reload {

    private Guns plugin;

    public Reload(Guns sw)
    {
        this.plugin = sw;
    }

    private static int counter = 0;

    // -------------------
    // |     SETTERS     |
    // -------------------

    public static void sendReload(final Guns g){

        final Player p = g.getPlayer();
        if(g.getReloadTime() == ReloadTimes.INSTANT){
            g.setMunition(g);
        }
        if(g.getReloadTime() == ReloadTimes.SUPER_FAST){
            p.setItemInHand(UtilItem.create(Material.BARRIER,  "Rechargement...", ""));
            p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 10.0F, 10.0F);
            TitleUtils.sendTitle(p, "§c§lRechargement...", "§7§l" + g.getGunItem().getItemMeta().getDisplayName());
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SCApi.plugin, new Runnable() {
                public void run() {
                    g.setMunition(g);
                    p.setItemInHand(g.createGun());
                }
            }, 20 * 3L);
        }


    }


    // -------------------
    // |     GETTERS     |
    // -------------------

    public static boolean isReloadNecessary(Guns g){

        if(Guns.munition.get(g) == 0){
            return true;
        }else{
            return false;
        }

    }


}
