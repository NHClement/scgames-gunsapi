package fr.sc.api.armes.Events;

import fr.sc.api.armes.Enums.BulletTypes;
import fr.sc.api.armes.Enums.GunType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

/**
 * Javadoc bientôt...
 */
public final class GunHitEvent extends Event implements Cancellable{

    private static final HandlerList handlers = new HandlerList();
    private GunType gun;
    private double Damage;
    private Player shooter;
    private Vector velocity;
    private BulletTypes bulletType;
    private double Explosion;
    private boolean cancelled = false;
    private Entity entity;

    public GunHitEvent(GunType g, Vector velocity, Player shooter, double Damage, BulletTypes bullet, double explosion, Entity entity) {
        gun = g;
        this.velocity = velocity;
        this.shooter = shooter;
        this.Damage = Damage;
        this.bulletType = bullet;
        this.Explosion = explosion;
        this.entity = entity;
    }

    // -------------------
    // |     SETTERS     |
    // -------------------

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public void setGun(GunType gun) {
        this.gun = gun;
    }

    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }

    public void setDamage(double Damage) {
        this.Damage = Damage;
    }

    public void setBulletType(BulletTypes bullet) {
        this.bulletType = bullet;
    }

    public void setExplosion(double explosion) {
        this.Explosion = explosion;
    }

    // -------------------
    // |     GETTERS     |
    // -------------------

    public boolean isCancelled() {
        return cancelled;
    }

    public GunType getGun() {
        return gun;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public Player getShooter() {
        return shooter;
    }

    public double getDamage() {
        return Damage;
    }

    public BulletTypes getBulletType() {
        return bulletType;
    }

    public double getExplosion() {
        return Explosion;
    }

    public Entity getBullet() {
        return this.entity;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
