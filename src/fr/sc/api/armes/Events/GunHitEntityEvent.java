package fr.sc.api.armes.Events;

import fr.sc.api.armes.Enums.BulletTypes;
import fr.sc.api.armes.Enums.GunType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

/**
 * Javadoc bientôt...
 */
public class GunHitEntityEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private GunType gun;
    private double Damage;
    private Player shooter;
    private Vector velocity;
    private BulletTypes bulletType;
    private double Explosion;
    private boolean cancelled = false;
    private Entity entity;
    private Entity bulletEntity;

    public GunHitEntityEvent(GunType g, Vector velocity, Player shooter, double Damage, BulletTypes bullet, double explosion, Entity entity, Entity bulletEntity)
    {
        this.gun = g;
        this.velocity = velocity;
        this.shooter = shooter;
        this.Damage = Damage;
        this.bulletType = bullet;
        this.Explosion = explosion;
        this.entity = entity;
        this.bulletEntity = bulletEntity;
    }

    // -------------------
    // |     SETTERS     |
    // -------------------

    public void setCancelled(boolean cancel)
    {
        this.cancelled = cancel;
    }

    public void setGun(GunType gun)
    {
        this.gun = gun;
    }

    public void setVelocity(Vector velocity)
    {
        this.velocity = velocity;
    }

    public void setDamage(double Damage)
    {
        this.Damage = Damage;
    }

    public void setBulletType(BulletTypes bullet)
    {
        this.bulletType = bullet;
    }

    public void setExplosion(double explosion)
    {
        this.Explosion = explosion;
    }

    // -------------------
    // |     GETTERS     |
    // -------------------

    public boolean isCancelled()
    {
        return this.cancelled;
    }

    public Entity getEntity()
    {
        return this.entity;
    }

    public GunType getGun()
    {
        return this.gun;
    }

    public Vector getVelocity()
    {
        return this.velocity;
    }

    public Player getShooter()
    {
        return this.shooter;
    }

    public double getDamage()
    {
        return this.Damage;
    }

    public BulletTypes getBulletType()
    {
        return this.bulletType;
    }

    public double getExplosion()
    {
        return this.Explosion;
    }

    public Entity getBullet()
    {
        return this.bulletEntity;
    }





    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
